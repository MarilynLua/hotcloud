package io.hotcloud.common.api.activity;

/**
 * @author yaolianhua789@gmail.com
 **/
public enum ActivityAction {
    //
    Create,
    Update,
    Delete
}
