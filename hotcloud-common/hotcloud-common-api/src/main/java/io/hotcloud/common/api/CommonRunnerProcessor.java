package io.hotcloud.common.api;

public interface CommonRunnerProcessor {

    void execute();
}
