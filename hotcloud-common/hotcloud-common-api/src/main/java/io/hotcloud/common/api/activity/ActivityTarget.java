package io.hotcloud.common.api.activity;

/**
 * @author yaolianhua789@gmail.com
 **/
public enum ActivityTarget {
    //
    Git_Clone,
    BuildPack,
    Instance_Template,
    Application
}
