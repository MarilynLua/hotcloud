package io.hotcloud.buildpack.api.core;

public enum ImageBuildStatus {
        //
        Ready,
        Active,
        Succeeded,
        Failed,
        Unknown

}