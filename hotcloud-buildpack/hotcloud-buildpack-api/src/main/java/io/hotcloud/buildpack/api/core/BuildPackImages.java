package io.hotcloud.buildpack.api.core;

public enum BuildPackImages {
    //
    Kaniko,
    //
    Git,
    //
    Alpine,
    //
    Java11

}
