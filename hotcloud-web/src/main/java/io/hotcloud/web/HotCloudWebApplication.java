package io.hotcloud.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author yaolianhua789@gmail.com
 **/
@SpringBootApplication
public class HotCloudWebApplication {
    public static void main(String[] args) {
        SpringApplication.run(HotCloudWebApplication.class, args);
    }
}
