package io.hotcloud.web.mvc;

/**
 * @author yaolianhua789@gmail.com
 **/
public final class WebConstant {

    public static final String USER = "user";

    public static final String HOTCLOUD_ENDPOINT = "endpoint";
    public static final String RESPONSE = "response";
    public static final String TEMPLATES = "templates";
    public static final String STATISTICS = "statistics";
    public static final String ACTIVITIES = "activities";
    public static final String AUTHORIZATION = "authorization";
    public static final String MESSAGE = "message";

    public static final String VIEW_LIST = "list";
    public static final String VIEW_EDIT = "edit";

    public static final String VIEW_DETAIL = "detail";

}
