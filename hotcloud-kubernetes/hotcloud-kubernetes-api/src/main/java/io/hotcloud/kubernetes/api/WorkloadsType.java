package io.hotcloud.kubernetes.api;

/**
 * @author yaolianhua789@gmail.com
 **/
public enum WorkloadsType {
    //
    Deployment,
    //
    DaemonSet,
    //
    Job,
    //
    CronJob,
    //
    StatefulSet,
    //
    Pod

}
