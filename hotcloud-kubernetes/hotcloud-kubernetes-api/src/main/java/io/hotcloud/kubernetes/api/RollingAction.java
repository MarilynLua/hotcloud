package io.hotcloud.kubernetes.api;

/**
 * @author yaolianhua789@gmail.com
 **/
public enum RollingAction {
    //
    PAUSE,
    //
    RESTART,
    //
    RESUME,
    //
    UNDO,

}
