package io.hotcloud.kubernetes.model.pod;

import lombok.Data;

/**
 * @author yaolianhua789@gmail.com
 **/
@Data
public class ImagePullSecret {

    private String name;
}
