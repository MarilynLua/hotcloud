package io.hotcloud.application.api.core;

public interface ApplicationInstanceParameterChecker {

    ApplicationInstance check (ApplicationForm form);
}
