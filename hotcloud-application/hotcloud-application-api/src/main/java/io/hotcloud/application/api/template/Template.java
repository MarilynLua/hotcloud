package io.hotcloud.application.api.template;

/**
 * @author yaolianhua789@gmail.com
 **/
public enum Template {
    //
    Mongodb,
    Minio,
    Redis,
    RedisInsight,
    Mysql,
    Rabbitmq
}
